<?php

namespace Solnet\Elements;

use Silvershop\HasOneField\HasOneButtonField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\TextField;

class ElementImageText extends CustomBaseElement
{
    private static $table_name = 'ElementImageText';
    private static $singular_name = 'Image Text Content';
    private static $plural_name = 'Image Text Content';
    private static $description = 'Image Text Content Module';

    private static $icon = 'font-icon-block-banner';

    private static $db = [
        'Content' => 'HTMLText',
        'CTAText' => 'Varchar(100)',
        'PanelPosition' => 'Enum("left,right", "right")'
    ];

    private static $has_one = [
        'CTALink' => 'SilverStripe\CMS\Model\SiteTree',
        'Background' => 'Solnet\Elements\Backgrounds\BackgroundMedia',
    ];

    public function getType()
    {
        return _t('Elements.SolnetImageText_BlockType', 'Image Text Content');
    }


    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName(array('BackgroundID'));

        $fields->addFieldsToTab(
            'Root.Main',
            [
                HTMLEditorField::create(
                    'Content',
                    _t('Elements.SolnetImageText_Content_Title', 'Content')
                ),
                TextField::create(
                    'CTAText',
                    _t('Elements.SolnetImageText_CTAText_Title', 'CTA Text')
                ),
                OptionsetField::create(
                    'PanelPosition',
                    _t('Elements.SolnetImageText_PanelPosition_Title', 'Text Panel Position'),
                    $this->dbObject('PanelPosition')->enumValues()
                )
            ]
        );

        if ($this->exists()) {
            $fields->addFieldToTab(
                'Root.Main',
                HasOneButtonField::create(
                    'Background',
                    _t('Elements.SolnetImageText_Background_Title', 'Background'),
                    $this
                )
            );
        } else {
            $fields->addFieldToTab(
                'Root.Main',
                LiteralField::create(
                    'SavingTip',
                    _t(
                        'Elements.SolnetImageText_SavingTip',
                        '<p class="message warning">Please save to see more options.</p>'
                    )
                )
            );
        }

        return $fields;
    }
}
